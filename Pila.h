using namespace std;

#ifndef PILA_H
#define PILA_H
// Declaración de la estructura "Pila"
class Pila{
    // Como privado se establecen los atributos de la pila
    private:
        int tope = 0;
        int *datos = NULL;
        int max = 0;
        bool band;
    public:
    // De manera publica se establecen los metodos
        Pila(int max);
        void pila_vacia();
        void pila_llena();
        void push(int dato);
        void pop();
        // Se decide agregar un metodo para imprimir la pila
        void print_pila();
};
#endif
