#include <iostream>
#include "Pila.h"
using namespace std;
// Se genera la función "menu" la cual se encarga de mostar las opciones al usuario
void menu(){
        cout << "¿Que es lo que desea hacer?" << endl;
        cout << "-------------------" << endl;
        cout << "Agregar/push \t[1]" << endl;
        cout << "Remover/Pop \t[2]" << endl;
        cout << "Ver pila \t[3]" << endl;
        cout << "Salir \t\t[0]" << endl;
        cout << "-------------------" << endl;
}
// Función principal del programa
int main(int argc, char* argv[]){
    /* Se declaran las variables: "max", 
    que se encarga de establecer la cantidad de elementos 
    maximos a contener por la pila, "dato", como placeholder del 
    valor a agregar a la pila y "opcion" la cual contendrá la 
    acción a realizar y por ende el metodo a utilizar*/
    string max, dato, opcion;
    /* El boleano on es la variable de control 
    para el ciclo principal del programa */
    bool on = true;
    // Se pide al usuario el tamaño maximo de la pila
    cout << "Ingrese tamaño Máximo de la Pila: ", getline(cin, max); 
    // Se instancia Pila con el maximo entregado por el usuario
    Pila pila = Pila(stoi(max));
    // Ciclo pincipal del programa
    while (on) {
        // Se limpia la terminal para dar un ui más amigable
        system("clear");
        // Se llama a menú para mostrar las opciones del programa
        menu();
        /* Se pone el getline en conjunto con la salida 
        para asemejarse a lo pedido por el cliente 
        en el documento del laboratorio*/
        cout << "Opción: ", getline(cin, opcion);
        system("clear");
        // Se utiliza la estructura switch para el llamado de los metodos
        switch (stoi(opcion)) {
        case 1:
            cout << "Ingrese valor: ", getline(cin, dato);
            pila.push(stoi(dato));
            break;
        case 2:
            pila.pop();
            break;
        case 3:
            pila.print_pila();
            break;
        case 0:
            cout << "Gracias por preferirnos. Hasta pronto!" << endl;
            on = false;
            break;
        default:
            cout << "Ingrese una selección valida del menú por favor" << endl;
            break;
        }
    }
    return 0;
}
