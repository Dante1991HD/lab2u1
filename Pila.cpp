#include <iostream>
#include "Pila.h"
using namespace std;
// Constructor usa "max" ingresado por usuario para la creación de la instancia
Pila::Pila(int max){
    this -> max = max;
    this -> tope = 0;
    this -> datos = new int[max];
    bool band;
}
// Atributo "band" toma valores segun tope
void Pila::pila_llena(){
    this->band = this->tope == max;
}

void Pila::pila_vacia(){
    this->band = this->tope == 0;
}
/* Método "push" ingresa "dato" al arreglo "datos" si pila tiene espacio
de lo contrario le hace saber al usuario, usa la variable tope como pivote
y se incremente en 1*/
void Pila::push(int dato){
    pila_llena();
    if(this -> band){
        cout << "Desbordamiento, pila llena" << endl;
    }
    else{
        this -> tope += 1;
        this -> datos[tope] = dato;
    }
    cout << "Presione enter para continuar ... ", cin.get();
}
/* Método "push" elimina "dato" del arreglo "datos" si pila tiene alguno
restante de lo contrario le hace saber al usuario, usa la variable topa
como pivote y se disminuye en 1*/
void Pila::pop(){
    pila_vacia();
    if(this -> band){
        cout << "Subdesbordamiento, Pila vacı́a" << endl;
    }
    else{
        cout << "Elemento eliminado: " << this->datos[tope] << endl;
        this -> tope -= 1;
    }
    cout << "Presione enter para continuar ... ", cin.get();
}
/* Se adiciona el presente metodo para mostrar a usuario los datos 
que contiene la pila, para hacerlo toma tope como inicio y resta de a uno
hasta llegar a 0*/
void Pila::print_pila(){
    pila_vacia();
    if(!this -> band){
        for(int i = this -> tope; i > 0; i--){
            cout << "[" << this -> datos[i] << "]" << endl;
        }
    }
    else{
        cout << "La pila se encuentra vacía" << endl;
    }
    cout << "Presione enter para continuar ... ", cin.get();
}