# Laboratorio II UI

## Descripción del programa 
El programa permite la generación de una "Pila", la cual es una estructura de datos de tipo "LIFO", por su sigla en ingles, Last In First Out, el usuario puede establecer la cantidad de valores a ingresar, los cuales serán numéricos de tipo entero, posteriormente puede escoger si ingresar datos, eliminarlos o mostrar en pantalla todos lo valores contenidos en la pila, sin mencionar una opción para cerrar el programa.

## Requerimientos para utilizar el programa
* Sistema operativo Linux
* Compilador make

En caso de no contar con make:

### ¿Como instalar make?
Para instalar make, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install make
```
Luego ingresar clave de usuario el sistema procederá a descargar e instalar el compilador. Una vez termine la descarga, su compilador estará listo para usar.

## Compilación y ejecución del programa
* Abrir una terminal en el directorio donde se ubican los archivos del programa e ingresar el siguiente comando.
```
make
```
* Luego de completada la compilación utilice el siguiente comando.
```
./programa
```

## Funcionamiento del programa
El programa pedirá inicialmente 1 parámetro, este será el tamaño máximo de valores de la "Pila" a generar.
* Luego de ingresar el parámetro aparecerá un menú como el siguiente.
```
¿Que es lo que desea hacer?
-------------------
Agregar/push [1]
Remover/Pop  [2]
Ver pila     [3]
Salir        [0]
-------------------
Opción: 
```

* **Debes ingresar un número entero de los que aparecen, es decir, _1, 2, 3 o 0_ y presionar la tecla **enter**.**
### Opción 1 - Agregar/push
```
Opción: 1
```
* Esta opción permite agregar valores a la "Pila" hasta que esta se llene de la siguiente manera.

```
Ingrese valor: 36
Presione enter para continuar ...
```

* Para continuar presionar la tecla **ENTER**.

### Opción 2 - Remover/Pop

```
Opción: 2
```

* Esta opción permite eliminar de la "Pila" el ultimo valor hasta que esta se vacíe de la siguiente manera.

```
Elemento eliminado: 7 
Presione enter para continuar ... 
```

* Para continuar presionar la tecla **ENTER**.

### Opción 3 - Ver pila

```
Opción: 3
```

* Esta opción genera una salida con todos los datos válidamente ingresados del último al primero según la descripción del programa.

```
[7]
[42]
[69]
Presione enter para continuar ...
```

* Para continuar presionar la tecla **ENTER**.

### Opción 0 - Salir

```
Opción: 0
```

* Esta opción da termino al ciclo principal del programa efectivamente cerrándolo, genera la siguiente salida.

```
Gracias por preferirnos. Hasta pronto!
```

## Autor
* Dante Aguirre
* Correo: daguirre12@alumnos.utalca.cl

